import string
from turtle import clear

# A class to represent a Matrix
class Matrix(object):
    def __init__(self, matrix_string):
        #Empty list to hold things
        matrix_data = []
        #parse string into array
        arr = matrix_string.split("\n")

        # for each item in list
        for row in arr:
            arr2 = row.split(" ")

        # create place to hold current row
            current_row = []
            for num in arr2:
                #convert it to an int
                num_int = int(num)

                #add it to rows
                current_row.append(num_int)

            matrix_data.append(current_row)

        #save data in here for later
        self.matrix_data = matrix_data

    def row(self, index):
        return self.matrix_data[index]

    def column(self, index):
        # created list to store the column.
        current_column = []
        # loop through each row.
        for row in self.matrix_data:
        #   get the value from the row
            num_int = row[index]
        #   append the value into the column
            current_column.append(num_int)
        # return the column
        return current_column